import 'package:flutter/material.dart';
import 'package:flutterappchallenge06/pages/DashBoard.dart';
import 'package:flutterappchallenge06/pages/LoadingPage.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: LoadingPage(),
  ));
}
