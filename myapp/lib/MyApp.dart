import 'package:flutter/material.dart';
import 'package:intl/intl.dart' as intl;

class MyApp extends StatefulWidget{
  String name;
  int age;
  MyApp({this.name, this.age});

  @override
  State<StatefulWidget> createState(){
    return _MyAppSate();
  }
}

class _MyAppSate extends State<MyApp> with WidgetsBindingObserver{
  String _email;
  final emailEditingController = TextEditingController();

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if(state == AppLifecycleState.paused){
      print("App is in background mode");
    }
    else if (state == AppLifecycleState.resumed){
      print("App is in foreground mode");
    }
  }
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    print("Run initState()");
  }
  @override
  void dispose() {
    super.dispose();
    emailEditingController.dispose();
    WidgetsBinding.instance.removeObserver(this);
    print("Run dipose()");
  }
  @override
  Widget build(BuildContext context) {
    DateTime now = new DateTime.now();
    return MaterialApp(
      title: "Food App",
      home: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                child: TextField(
                  controller: emailEditingController,
                  onChanged: (text){
                    this.setState(() {
                      _email = text;
                    });
                  },
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(20.0)
                      ),
                    ),
                    labelText: "Enter your name",
                  ),
                ),
              ),
              Text(
                "Changed: ${this._email}",
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold, color: Colors.green),
                textDirection: TextDirection.ltr,
              ),
              Text(
                intl.DateFormat("yyyy-MM-dd HH:mm:ss").format(now),
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold, color: Colors.orange),
                textDirection: TextDirection.ltr,
              ),
              Text(
                intl.DateFormat("yyyy/MM/dd").format(now),
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold, color: Colors.orange),
                textDirection: TextDirection.ltr,
              ),
              Text(
                intl.NumberFormat("###.0#","en_US").format(12.1234),
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold, color: Colors.orange),
                textDirection: TextDirection.ltr,
              ),
            ],
          )
        ),
      ),
    );
  }
}