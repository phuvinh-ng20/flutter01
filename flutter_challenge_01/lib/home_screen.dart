import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _HomeScreenState();
  }
}

class _HomeScreenState extends State<HomeScreen> {
  var _navigatorKey = new GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
    navigatorKey: _navigatorKey,
    theme: ThemeData(fontFamily: 'RobotoSlab'),
      debugShowCheckedModeBanner: false,
      title: "Challenge 01",
      home: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 0,
            leading: Builder(
              builder: (context) => IconButton(
                icon: Icon(
                  Icons.menu,
                  color: Colors.green,
                ),
                onPressed: () {
                  Scaffold.of(context).openDrawer();
                },
              ),
            )
          ),
          body: SafeArea(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: double.infinity,
                      padding: EdgeInsets.only(left: 20, bottom: 20, right: 20),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius:
                        BorderRadius.vertical(bottom: Radius.circular(30.0)),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Find Your",
                            style: TextStyle(color: Colors.black, fontSize: 25.0),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            "Inspiration",
                            style: TextStyle(color: Colors.black, fontSize: 35.0),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            padding: EdgeInsets.all(5.0),
                            decoration: BoxDecoration(
                                color: Color.fromRGBO(229, 229, 229, 1),
                                borderRadius: BorderRadius.circular(30)),
                            child: TextField(
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                prefixIcon: Icon(
                                  Icons.search,
                                  color: Colors.green,
                                ),
                                hintText: "What're you looking for?",
                                hintStyle:
                                TextStyle(color: Colors.grey, fontSize: 15.0),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Promo Today",
                            style: TextStyle(
                                color: Colors.black, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            height: 200,
                            child: ListView(
                              scrollDirection: Axis.horizontal,
                              children: <Widget>[
                                promoCard('assets/images/01.jpeg'),
                                promoCard('assets/images/02.jpeg'),
                                promoCard('assets/images/03.jpeg'),
                                promoCard('assets/images/04.jpeg'),
                                promoCard('assets/images/05.jpeg'),
                                promoCard('assets/images/06.jpeg'),
                              ],
                            ),
                          ),
                          SizedBox(height: 20,),
                          Container(
                            height: 250,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage('assets/images/bottom.jpg'),
                                    fit: BoxFit.cover
                                ),
                                borderRadius: BorderRadius.circular(20.0),
                                gradient: LinearGradient(
                                    begin: Alignment.bottomRight,
                                    stops: [
                                      0.1,
                                      0.9
                                    ],
                                    colors: [
                                      Colors.black.withOpacity(0.8),
                                      Colors.black.withOpacity(0.1),
                                    ])),
                            child: Align(
                              alignment: Alignment.bottomCenter,
                              child: Text("Best Practice", style: TextStyle(color: Colors.white, fontSize: 20.0),),
                            ),),
                        ],
                      ),
                    ),
                  ],
                ))),
        drawer: Drawer(
          child: SafeArea(
            child: ListView(
              children: <Widget>[
                DrawerHeader(
                  decoration: BoxDecoration(
                  ),
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Material(
                          child: Container(
                            height: 100,
                            width: 100,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                            ),
                            child: Icon(Icons.star, size: 120, color: Colors.green,),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                CustomListTitle(icon: Icons.person,title: "Personal",OnTap: () => {_navigatorKey.currentState.pop()},),
                CustomListTitle(icon: Icons.notifications,title: "Notification",OnTap: () => {_navigatorKey.currentState.pop()},),
                CustomListTitle(icon: Icons.settings,title: "Setting",OnTap: () => {_navigatorKey.currentState.pop()},),
                CustomListTitle(icon: Icons.info,title: "Info",OnTap: () => {_navigatorKey.currentState.pop()},),
                CustomListTitle(icon: Icons.forward,title: "Log Out",OnTap: () => {_navigatorKey.currentState.pop()},),
              ],
            )
          )
        ),
    )
    );
  }

  Widget promoCard(image) {
    return AspectRatio(
      aspectRatio: 2.6 / 3,
      child: Container(
        margin: EdgeInsets.only(right: 15.0),
        decoration: BoxDecoration(
            color: Colors.green,
            borderRadius: BorderRadius.circular(20.0),
            image: DecorationImage(
                image: AssetImage("${image}"), fit: BoxFit.cover)),
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
              gradient: LinearGradient(begin: Alignment.bottomRight, stops: [
                0.1,
                0.9
              ], colors: [
                Colors.black.withOpacity(0.8),
                Colors.black.withOpacity(0.1),
              ])),
        ),
      ),
    );
  }
}

class CustomListTitle extends StatelessWidget {
  IconData icon;
  String title;
  Function OnTap;

  CustomListTitle({this.icon, this.title, this.OnTap});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.zero,
      child: InkWell(
        splashColor: Colors.grey,
        onTap: OnTap,
        child: Container(
          padding: EdgeInsets.only(left: 15),
          height: 60,
          child: Row(
            children: <Widget>[
              Icon(icon, size: 24,),
              SizedBox(width: 30,),
              Text(title, style: TextStyle(fontSize: 18),)
            ],
          ),
        ),
      ),
    );
  }
}

