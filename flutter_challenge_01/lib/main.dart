import 'package:flutter/material.dart';
import 'package:flutterchallenge01/home_screen.dart';

import 'home_screen.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: HomeScreen(),
  ));
}
