import 'package:flutter/material.dart';
import 'package:flutter_challenge_03/animations/FadeAnimation.dart';
import 'package:flutter_challenge_03/pages/HomePage.dart';
import 'package:page_transition/page_transition.dart';

class StarterPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _StarterPageState();
  }
}

class _StarterPageState extends State<StarterPage> with TickerProviderStateMixin, WidgetsBindingObserver {
  AnimationController _animationController;
  Animation<double> _animation;

  bool _textVisible = true;

  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration:  Duration(milliseconds: 100)
    );

    _animation = Tween<double>(
      begin: 1.0,
      end: 1.0
    ).animate(_animationController);

    WidgetsBinding.instance.addObserver(this);
    print('init');
    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    WidgetsBinding.instance.removeObserver(this);
    print('dispose');
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch(state){
      case AppLifecycleState.resumed:
        print('resume');
        break;
      case AppLifecycleState.paused:
        print('paused');
        break;
      case AppLifecycleState.inactive:
        print('inactive');
        break;
      case AppLifecycleState.detached:
        print('detached');
        break;
    }
    super.didChangeAppLifecycleState(state);
  }

  void _onTap(){
    setState(() {
      _textVisible = false;
    });

    _animationController.forward().then((f) =>
        Navigator.push(context, PageTransition(type: PageTransitionType.fade, child: HomePage()))
    );
  }
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      theme: ThemeData(fontFamily: 'Roboto'),
      debugShowCheckedModeBanner: false,
      title: "Challenge 03",
      home: Scaffold(
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/starter-image.jpg'),
              fit: BoxFit.cover
            )
          ),
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.bottomCenter,
                colors: [
                  Colors.black.withOpacity(0.9),
                  Colors.black.withOpacity(0.8),
                  Colors.black.withOpacity(0.2),
                ]
              )
            ),
            child: Padding(
              padding: EdgeInsets.all(20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  FadeAnimation(0.5,Text("Taking Order For Faster Delivery",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 65.0),)),
                  SizedBox(height: 30,),
                  FadeAnimation(1,Text("See restaurant near by adding location", style: TextStyle(color: Colors.white, fontSize: 20,height: 1.9))),
                  SizedBox(height: 150,),
                  FadeAnimation(1.5,
                    ScaleTransition(
                      scale: _animation,
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                colors: [
                                  Colors.yellow,
                                  Colors.orange
                                ]
                            )
                        ),
                        child: AnimatedOpacity(
                          opacity: _textVisible ? 1.0 : 0.0,
                          duration: Duration(milliseconds: 50),
                          child: MaterialButton(
                            height: 60,
                            minWidth: double.infinity,
                            child: Text("Start",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 24),),
                            onPressed: (){
                              _onTap();
                            },
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 30,),
                  AnimatedOpacity(
                    opacity: _textVisible ? 1.0 : 0.0,
                    duration: Duration(milliseconds: 50),
                    child: FadeAnimation(2,
                        Align(
                          child: Text("Now Deliver To Your Door 24/7", style: TextStyle(color: Colors.grey, fontSize: 15)),
                        )),
                  ),
                  SizedBox(height: 30,)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}