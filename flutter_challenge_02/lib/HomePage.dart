import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterchallenge02/FadeAnimation.dart';

class HomePage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin{
  PageController _pageController;

  void _onScroll(){
  }
  
  @override
  void initState() {
    // TODO: implement initState
    _pageController = PageController(
      initialPage: 0,
    )..addListener(_onScroll);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Challenge 02",
      home: Scaffold(
        body: PageView(
          controller: _pageController,
          children: <Widget>[
            _makePage(
                page: "1",
                title: "Yosemite National Park",
                description: "Yosemite National Park is in California’s Sierra Nevada mountains. It’s famed for its giant, ancient sequoia trees, and for Tunnel View, the iconic vista of towering Bridalveil Fall and the granite cliffs of El Capitan and Half Dome.",
                image: 'assets/images/01.jpeg'),
            _makePage(
                page: "2",
                title: "Golden Gate Bridge",
                description: "The Golden Gate Bridge is a suspension bridge spanning the Golden Gate, the one-mile-wide strait connecting San Francisco Bay and the Pacific Ocean.",
                image: 'assets/images/02.jpeg'),
            _makePage(
                page: "3",
                title: "Sedona",
                description: "Sedona is regularly described as one of America's most beautiful places. Nowhere else will you find a landscape as dramatically colorful.",
                image: 'assets/images/03.jpeg'),
          ],
        ),
      ),
    );
  }

  Widget _makePage({page,image,title,description}){
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('${image}'),
          fit: BoxFit.cover
        ),
      ),
      child: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.bottomRight,
                stops: [0.3,0.9],
                colors: [
                  Colors.black.withOpacity(0.9),
                  Colors.black.withOpacity(0.2),
                ]
            )
        ),
        child: Padding(
          padding: EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.baseline,
                textBaseline: TextBaseline.alphabetic,
                children: <Widget>[
                  FadeAnimation(
                    delay: 1,
                    child: Text("${page}", style: TextStyle(fontSize: 30.0,color: Colors.white, fontWeight: FontWeight.bold),),
                  ),
                  FadeAnimation(
                    delay: 1,
                    child: Text("/4", style: TextStyle(fontSize: 15.0,color: Colors.white, fontWeight: FontWeight.bold),),
                  )
                ],
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    FadeAnimation(
                        delay: 1,
                        child: Text("${title}", style: TextStyle(fontSize: 40.0,color: Colors.white, fontWeight: FontWeight.bold),)
                    ),
                    SizedBox(height: 10,),
                    Row(
                      children: <Widget>[
                        FadeAnimation(
                          delay: 1,
                          child: Container(
                            child: Icon(Icons.star, color: Colors.yellow, size: 15.0,),
                          ),
                        ),
                        FadeAnimation(
                          delay: 1,
                          child: Container(
                            child: Icon(Icons.star, color: Colors.yellow, size: 15.0,),
                          ),
                        ),
                        FadeAnimation(
                          delay: 1,
                          child: Container(
                            child: Icon(Icons.star, color: Colors.yellow, size: 15.0,),
                          ),
                        ),
                        FadeAnimation(
                          delay: 1,
                          child: Container(
                            child: Icon(Icons.star, color: Colors.yellow, size: 15.0,),
                          ),
                        ),
                        FadeAnimation(
                          delay: 1,
                          child: Container(
                            child: Icon(Icons.star_border, color: Colors.yellow, size: 15.0,),
                          ),
                        ),
                        FadeAnimation(
                          delay: 1,
                          child: Text("4.0",style: TextStyle(fontSize: 15,color: Colors.grey),),
                        ),
                        FadeAnimation(
                          delay: 1,
                          child: Text("(2300)",style: TextStyle(fontSize: 12,color: Colors.grey),),
                        ),
                      ],
                    ),
                    SizedBox(height: 30,),
                    Padding(
                      padding: const EdgeInsets.only(right: 150),
                      child: FadeAnimation(
                        delay: 1,
                        child: Text("${description}",style: TextStyle(fontSize: 14,color: Colors.grey,height: 1.9)),
                      )
                    ),
                    SizedBox(height: 30,),
                    FadeAnimation(
                      delay: 1,
                      child: Text("READ MORE",style: TextStyle(fontSize: 20,color: Colors.grey),),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}