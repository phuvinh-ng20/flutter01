import 'package:flutter/material.dart';
import 'package:flutterchallenge07/ButtonAnimation.dart';

void main() {
  runApp(MaterialApp(
    home: HomePage(),
  ));
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ButtonAnimation(Colors.pink[300], Colors.pink),
              SizedBox(height: 30,),
              ButtonAnimation(Colors.green[300], Colors.green),
              SizedBox(height: 30,),
              ButtonAnimation(Colors.deepOrange[300], Colors.deepOrange),
              SizedBox(height: 30,),
              ButtonAnimation(Colors.red[300], Colors.red),
              SizedBox(height: 30,),
            ],
          ),
        ),
      ),
    );
  }
}

