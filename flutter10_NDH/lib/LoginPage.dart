import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _LoginPageState();
  }
}

class _LoginPageState extends State<LoginPage>{
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _ContentController = TextEditingController();
  final _AmountController = TextEditingController();

  String _content;
  double _amount;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Flutter 10",
      home: Scaffold(
        key: _scaffoldKey,
        body: SafeArea(
          minimum: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
                decoration: BoxDecoration(
                  color: Colors.pinkAccent,
                  borderRadius: BorderRadius.circular(20)
                ),
                child: TextField(
                  decoration: InputDecoration(labelText: "Content", labelStyle: TextStyle(color: Colors.white), border: InputBorder.none),
                  controller: _ContentController,
                  onChanged: (text){
                    setState(() {
                      _content = text;
                    });
                  },
                ),
              ),
              TextField(
                decoration: InputDecoration(labelText: "Amount"),
                controller: _AmountController,
                onChanged: (text){
                  setState(() {
                    _amount = double.tryParse(text) ?? 0;
                  });
                },
              ),
              FlatButton(
                child: Text("Click here"),
                color: Colors.greenAccent,
                textColor: Colors.white,
                onPressed: (){
                  print("Content: ${_content} \nAmount: ${_amount.toString()}");
                  _scaffoldKey.currentState.showSnackBar(
                    SnackBar(content: Text("Content: ${_content} \nAmount: ${_amount.toString()}"),)
                  );
                },
              )
            ],
          ),
        ),
      )
    );
  }
}