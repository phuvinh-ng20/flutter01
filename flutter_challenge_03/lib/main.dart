import 'package:flutter/material.dart';
import 'package:flutter_challenge_03/pages/HomePage.dart';
import 'package:flutter_challenge_03/pages/StarterPage.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: StarterPage(),
    theme: ThemeData(fontFamily: 'Roboto'),
  ));
}

