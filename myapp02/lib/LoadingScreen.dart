import 'package:flutter/material.dart';

class LoadingScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoadingScreenState();
  }
}

class _LoadingScreenState extends State<LoadingScreen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Loading Screen",
      home: Scaffold(
        body: Table(
            border: TableBorder.all(),
            children: [
              TableRow(
                children: [
                Center(child: Text("Nguyen Phu Vinh",style: TextStyle(backgroundColor: Colors.green),),),
                Center(child: Text("1",style: TextStyle(backgroundColor: Colors.green),),),
                Center(child: Text("1",style: TextStyle(backgroundColor: Colors.green),),),
              ]),
              TableRow(children: [
                Center(child: Text("1",style: TextStyle(backgroundColor: Colors.green),),),
                Center(child: Text("1",style: TextStyle(backgroundColor: Colors.green),),),
                Center(child: Text("1",style: TextStyle(backgroundColor: Colors.green),),),
              ]),
            ],
          ),
      ),
    );
  }
}
