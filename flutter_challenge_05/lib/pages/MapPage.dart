import 'package:animator/animator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class MapPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MapPageState();
  }
}

class _MapPageState extends State<MapPage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      theme: ThemeData(fontFamily: 'Roboto'),
      title: "Flutter Challenge 05",
      home: Scaffold(
        body: Container(
          child: Stack(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/background.jpg'),
                    fit: BoxFit.cover
                  ),
                ),
                child: Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.bottomCenter,
                      colors: [
                        Colors.black.withOpacity(0.9),
                        Colors.black.withOpacity(0.3)
                      ]
                    )
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.all(10),
                        height: 300,
                        child: ListView(
                          scrollDirection: Axis.horizontal,
                          children: <Widget>[
                            makeItem(image: 'assets/images/place.jpg'),
                            makeItem(image: 'assets/images/place2.jpg'),
                            makeItem(image: 'assets/images/place3.jpg'),
                          ],
                        ),
                      ),
                      SizedBox(height: 50,)
                    ],
                  ),
                ),
              ),
              makePoint(top: 140, left: 60),
              makePoint(top: 190, left: 200),
              makePoint(top: 240, left: 100),
            ],
          ),
        ),
      ),
    );
  }

  Widget makePoint({top,left}){
    return Positioned(
      top: top,
      left: left,
      child: Container(
        height: 20,
        width: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.blue.withOpacity(0.3)
        ),
        child: Animator(
          duration: Duration(seconds: 1),
          tween: Tween(begin: 4.0 ,end: 6.0),
          cycles: 0,
          builder: (context, anim, child) => Center(
            child: Container(
              margin: EdgeInsets.all(anim.value),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.blue
              ),
            ),
          )
        ),
      ),
    );
  }

  Widget makeItem({image}){
    return AspectRatio(
      aspectRatio: 2.2/2,
      child: Container(
        margin: EdgeInsets.only(left: 30),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(40),
          color: Colors.white
        ),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 5, top: 20),
                  height: 130,
                  width: 130,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(40),
                    image: DecorationImage(
                      image: AssetImage(image),
                      fit: BoxFit.cover
                    )
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 15, top: 20),
                  height: 30,
                  width: 100,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.grey[500]
                  ),
                  child: Align(
                    alignment: Alignment.center,
                    child: Text("2.1 km", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Colors.white),),
                  )
                )
              ],
            ),
            SizedBox(height: 10,),
            Container(
              margin: EdgeInsets.only(left: 20, right: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text("Golde Gate Bridg", style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold),),
              )
            ),
            Container(
              margin: EdgeInsets.only(right: 30),
              child: Align(
                alignment: Alignment.centerRight,
                child: Icon(Icons.star_border, size: 40, color: Colors.orange,),
              ),
            ),
          ],
        ),
      ),
    );
  }
}