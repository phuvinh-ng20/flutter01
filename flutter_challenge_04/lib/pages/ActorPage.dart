import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_challenge_04/animation/FadeAnimation.dart';

class ActorPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ActorPageState();
  }
}

class _ActorPageState extends State<ActorPage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Actor Page",
      home: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.black,
          body: Stack(
            children: <Widget>[
              CustomScrollView(
                slivers: <Widget>[
                  SliverAppBar(
                    backgroundColor: Colors.black,
                    expandedHeight: 450,
                    flexibleSpace: FlexibleSpaceBar(
//                      collapseMode: CollapseMode.pin,
                      background: Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage('assets/images/emma.jpg'),
                            fit: BoxFit.cover
                          ),
                        ),
                        child: Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.bottomRight,
                              colors: [
                                Colors.black.withOpacity(1),
                                Colors.black.withOpacity(0.2)
                              ]
                            )
                          ),
                          child: Padding(
                            padding: EdgeInsets.all(20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                FadeAnimation(0.5, Text("Emma Waston", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 40),),),
                                SizedBox(height: 30,),
                                Row(
                                  children: <Widget>[
                                    FadeAnimation(1,Text("69 Videos", style: TextStyle(color: Colors.grey, fontSize: 18),),),
                                    SizedBox(width: 80,),
                                    FadeAnimation(1,Text("690K Subscribers", style: TextStyle(color: Colors.grey, fontSize: 18),),),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SliverList(
                    delegate: SliverChildListDelegate([
                      Padding(
                        padding: EdgeInsets.all(20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            FadeAnimation(1,Text("Emma Charlotte Duerre Watson was born in Paris, France, to English parents, Jacqueline Luesby and Chris Watson, both lawyers. She moved to Oxfordshire when she was five, where she attended the Dragon School.",
                              style: TextStyle(color: Colors.grey, fontSize: 16),),),
                            SizedBox(height: 30,),
                            FadeAnimation(1,Text("Born",style: TextStyle(color: Colors.white, fontSize: 24, fontWeight: FontWeight.bold),),),
                            SizedBox(height: 10,),
                            FadeAnimation(1,Text("April, 15th 1990, Paris, France",style: TextStyle(color: Colors.grey, fontSize: 16, fontWeight: FontWeight.normal),),),
                            SizedBox(height: 30,),
                            FadeAnimation(1,Text("Nationality",style: TextStyle(color: Colors.white, fontSize: 24, fontWeight: FontWeight.bold),),),
                            SizedBox(height: 10,),
                            FadeAnimation(1,Text("British",style: TextStyle(color: Colors.grey, fontSize: 16, fontWeight: FontWeight.normal),),),
                            SizedBox(height: 30,),
                            FadeAnimation(1,Text("Videos",style: TextStyle(color: Colors.white, fontSize: 24, fontWeight: FontWeight.bold),),),
                            SizedBox(height: 20,),
                            Container(
                              color: Colors.black,
                              height: 200,
                              child: ListView(
                                scrollDirection: Axis.horizontal,
                                children: <Widget>[
                                  makeItemVideo(image: 'assets/images/emma-1.jpg'),
                                  makeItemVideo(image: 'assets/images/emma-2.jpg'),
                                  makeItemVideo(image: 'assets/images/emma-3.jpg'),
                                ],
                              ),
                            ),
                            SizedBox(height: 100,)
                          ],
                        ),
                      )
                    ]),
                  ),
                ],
              ),
              Positioned.fill(
                bottom: 50,
                child: FadeAnimation(1,Container(
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      margin: EdgeInsets.only(left: 20, right: 20),
                      height: 50,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.orange
                      ),
                      child: Align(
                        child: Text("Follow", style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold),),
                      ),
                    ),
                  ),
                ),)
              )
            ],
          )
        ),
      ),
    );
  }

  Widget makeItemVideo({image}){
    return AspectRatio(
      aspectRatio: 1.75/1,
      child: Container(
        margin: EdgeInsets.only(right: 20),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          image: DecorationImage(
            image: AssetImage(image),
            fit: BoxFit.cover
          )
        ),
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.bottomCenter,
              colors: [
                Colors.black.withOpacity(0.6),
                Colors.black.withOpacity(0.2)
              ]
            )
          ),
          child: Padding(
            padding: EdgeInsets.all(20),
            child: Align(
              child: Icon(Icons.play_arrow, color: Colors.white, size: 40,),
            ),
          ),
        ),
      ),
    );
  }
}