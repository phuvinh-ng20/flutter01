import 'package:flutter/material.dart';
import 'package:flutterappchallenge06/animation/FadeAnimationRTL.dart';

class DashBoard extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _DashBoardState();
  }
}

class _DashBoardState extends State<DashBoard> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SafeArea(
        child: Scaffold(
          body: Column(
            children: <Widget>[
              Container(
                height: 150,
                child: Align(
                    child: FadeAnimationRTL(
                  1,
                  Text(
                    "DashBoard",
                    style: TextStyle(fontSize: 45, fontWeight: FontWeight.bold),
                  ),
                )),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Container(
                    color: Colors.grey[200],
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(left: 10, top: 10),
                          child: Align(
                              alignment: Alignment.centerLeft,
                              child: FadeAnimationRTL(
                                1.2,
                                Text(
                                  "Today",
                                  style: TextStyle(
                                      fontSize: 32,
                                      fontWeight: FontWeight.bold),
                                ),
                              )),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 30),
                          height: 150,
                          child: ListView(
                            scrollDirection: Axis.horizontal,
                            children: <Widget>[
                              FadeAnimationRTL(
                                1.4,
                                makeItem(
                                    color: Colors.blue,
                                    activity: "Steps",
                                    detail: "3500"),
                              ),
                              FadeAnimationRTL(
                                1.6,
                                makeItem(
                                    color: Colors.pink,
                                    activity: "Sports",
                                    detail: "25 Min"),
                              ),
                              FadeAnimationRTL(
                                  1.8,
                                  makeItem(
                                      color: Colors.deepOrange,
                                      activity: "Bicycle",
                                      detail: "5 Rounds"))
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 25,
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 10),
                          child: Align(
                              alignment: Alignment.centerLeft,
                              child: FadeAnimationRTL(
                                1.6,
                                Text(
                                  "Health Categories",
                                  style: TextStyle(
                                      fontSize: 32,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold),
                                ),
                              )),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        FadeAnimationRTL(
                          1.8,
                          Container(
                            margin: EdgeInsets.only(left: 30, right: 30),
                            height: 300,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: Colors.white),
                            child: ListView(
                              scrollDirection: Axis.vertical,
                              children: <Widget>[
                                FadeAnimationRTL(
                                  2,
                                  makeItemList(activity: "Activity"),
                                ),
                                FadeAnimationRTL(
                                  2.2,
                                  makeItemList(activity: "Activity"),
                                ),
                                FadeAnimationRTL(
                                  2.4,
                                  makeItemList(activity: "Activity"),
                                ),
                                FadeAnimationRTL(
                                  2.4,
                                  makeItemList(activity: "Activity"),
                                ),
                                FadeAnimationRTL(
                                  2.4,
                                  makeItemList(activity: "Activity"),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget makeItemList({activity}) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(30),
      decoration:
          BoxDecoration(border: Border(bottom: BorderSide(color: Colors.grey))),
      child: Text(
        activity,
        style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget makeItem({color, activity, detail}) {
    return AspectRatio(
      aspectRatio: 3 / 2,
      child: Container(
          margin: EdgeInsets.only(right: 30),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30), color: color),
          child: Padding(
            padding: EdgeInsets.only(left: 30, top: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  activity,
                  style: TextStyle(fontSize: 42, color: Colors.white),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  detail,
                  style: TextStyle(fontSize: 32, color: Colors.white),
                ),
              ],
            ),
          )),
    );
  }
}
