import 'package:flutter/material.dart';
import 'package:flutterappchallenge06/animation/FadeAnimationRTL.dart';
import 'package:flutterappchallenge06/pages/DashBoard.dart';
import 'package:page_transition/page_transition.dart';

class LoadingPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoadingPageState();
  }
}

class _LoadingPageState extends State<LoadingPage>
    with TickerProviderStateMixin {
  PageController _pageController;

  AnimationController rippleController;
  AnimationController scaleController;

  Animation<double> rippleAnimation;
  Animation<double> scaleAnimation;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _pageController = PageController(initialPage: 0);

    rippleController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));

    scaleController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300))
          ..addStatusListener((status) {
            if (status == AnimationStatus.completed) {
              Navigator.push(
                  context,
                  PageTransition(
                      type: PageTransitionType.fade, child: DashBoard()));
            }
          });

    rippleAnimation =
        Tween<double>(begin: 80.0, end: 90.0).animate(rippleController)
          ..addStatusListener((status) {
            if (status == AnimationStatus.completed) {
              rippleController.reverse();
            } else if (status == AnimationStatus.dismissed) {
              rippleController.forward();
            }
          });

    scaleAnimation =
        Tween<double>(begin: 1.0, end: 30.0).animate(scaleController);

    rippleController.forward();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Flutter Challenge 06",
      home: SafeArea(
        child: Scaffold(
            body: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/images/one.jpg'),
                    fit: BoxFit.cover),
              ),
              child: Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.centerLeft,
                        colors: [
                      Colors.black.withOpacity(0.4),
                      Colors.black.withOpacity(0.3)
                    ])),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 80, top: 100),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  FadeAnimationRTL(
                    1,
                    Text(
                      "Exercise 1",
                      style: TextStyle(
                          fontSize: 60,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  FadeAnimationRTL(
                    1.2,
                    Text(
                      "15",
                      style: TextStyle(
                          fontSize: 60,
                          fontWeight: FontWeight.bold,
                          color: Colors.yellow),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  FadeAnimationRTL(
                    1.4,
                    Text(
                      "Minutes",
                      style: TextStyle(
                          fontSize: 60,
                          fontWeight: FontWeight.normal,
                          color: Colors.white),
                    ),
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  FadeAnimationRTL(
                      1.6,
                      Text(
                        "3",
                        style: TextStyle(
                            fontSize: 60,
                            fontWeight: FontWeight.bold,
                            color: Colors.yellow),
                      )),
                  SizedBox(
                    height: 5,
                  ),
                  FadeAnimationRTL(
                    1.8,
                    Text(
                      "Exercises",
                      style: TextStyle(
                          fontSize: 60,
                          fontWeight: FontWeight.normal,
                          color: Colors.white),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Align(
                      child: FadeAnimationRTL(
                    2,
                    Text(
                      "Start the morning with your health",
                      style: TextStyle(fontSize: 40, color: Colors.white),
                    ),
                  )),
                  SizedBox(
                    height: 45,
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(bottom: 30),
              child: Align(
                  alignment: Alignment.bottomCenter,
                  child: FadeAnimationRTL(
                    2.2,
                    AnimatedBuilder(
                      animation: rippleAnimation,
                      builder: (context, child) => Container(
                        width: rippleAnimation.value,
                        height: rippleAnimation.value,
                        child: Container(
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.greenAccent.withOpacity(0.4)),
                          child: InkWell(
                            onTap: () {
                              scaleController.forward();
                            },
                            child: AnimatedBuilder(
                                animation: scaleAnimation,
                                builder: (context, child) => Transform.scale(
                                      scale: scaleAnimation.value,
                                      child: Container(
                                        margin: EdgeInsets.all(10),
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.greenAccent),
                                      ),
                                    )),
                          ),
                        ),
                      ),
                    ),
                  )),
            )
          ],
        )),
      ),
    );
  }
}
